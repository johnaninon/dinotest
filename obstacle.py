import pygame
import random
from defs import *

class Obstacle():

    def __init__(self, displayGame, x, y, speed):
        self.displayGame = displayGame
        self.state = obstacle_moving
        self.img = pygame.image.load(obstacle_file)
        self.img = pygame.transform.scale(self.img,(40,random.randint(40,130)))
        self.rect = self.img.get_rect()
        self.set_position(x, y)
        self.obs_speed = speed
        self.speed_state = obstacle_speed_slow

    def set_position(self, x, y):
        self.rect.left = x
        self.rect.bottom = y

    def move_position(self, dx, dy):
        self.rect.centerx += dx
        self.rect.centery += dy

    def draw(self):
        self.displayGame.blit(self.img, self.rect)

    def check_status(self):
        if self.rect.right < 0:
            self.state = obstacle_done
            #print('OBSTACLE_GONE')

    def check(self, dt, speed):

        if self.state == obstacle_moving:
            self.move_position(-(speed * dt), 0)
            self.draw()
            self.check_status()

    def add_new(self, x, speed):
        o1 = Obstacle(self.displayGame, x, 448, speed)
        self.obz.append(o1)

    def initialize(self):
        self.obz = []
        self.obs_gap = obstacle_gap

        '''--ADDS OBSTACLES IN SCREEN WHEN GAME STARTS--
        placed = obstacle_first
        while placed < width_game:
            self.add_new(placed)
            placed += obstacle_gap'''

    def update(self, dt, game_time):
        rightmost = 0 #assumption

        g_t = round(game_time/1000,1)
        if g_t != 0:
            if g_t % 10 == 0:
                if self.speed_state == obstacle_speed_slow:
                    self.obs_speed += 0.065
                    self.obs_gap += 100
                    if self.obs_gap > width_game:
                        self.obs_gap = width_game-1
                    self.speed_state = obstacle_speed_fast
            else:
                self.speed_state = obstacle_speed_slow
                    

        for o in self.obz:
            o.check(dt, self.obs_speed)
            if o.rect.left > rightmost: #it's okay to remove this tho
                rightmost = o.rect.left

        if rightmost < (width_game - self.obs_gap): #adds new obstacle after a gap
            self.add_new(width_game, self.obs_speed)

        

        self.obz = [o for o in self.obz if o.state == obstacle_moving] #removes obstacles_gone from the array