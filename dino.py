import pygame
import random
from defs import *
#test
class Dino():
    def __init__(self,displayGame):
        self.displayGame = displayGame
        self.state = dino_alive
        self.img = pygame.image.load(dino_file)
        self.rect = self.img.get_rect()
        self.speed = dino_start_speed
        self.time_lived = 0
        self.set_position(dino_start_X,dino_start_Y)
        self.status = 0

    def set_position(self,x,y):
        self.rect.centerx = x
        self.rect.centery = y

    def move(self,dt):
        if self.status == 1:
            distance = 0
            new_speed = 0

            distance = (self.speed * dt) + (0.5 * dt * dt * gravity)
            new_speed = self.speed + (gravity * dt)
            
            self.rect.centery += distance
            self.speed = new_speed
            if self.rect.centery > 428:
                self.rect.centery = 428
                self.status = 0
        
    def jump(self):
        if self.status == 0:
            self.speed = dino_start_speed
            self.status = 1

    def draw(self):
        self.displayGame.blit(self.img,self.rect)

    def check_status(self, obz):
        if self.rect.top > height_game:
            self.state = dino_dead
        else:
            self.check_hits(obz)

    def check_hits(self,obz):
        for o in obz:
            if o.rect.colliderect(self.rect):
                self.state = dino_dead
                break

    def update(self, dt, obz):
        if self.state == dino_alive: 
            self.time_lived += dt
            self.move(dt)
            self.draw()
            self.check_status(obz) 

class DinoGroup():
    def __init__(self,displayGame):
        self.displayGame = displayGame
        self.dinos = []
        self.create_new_generation()

    def create_new_generation(self):
        self.dinos = []
        for i in range(0,generation_size):
            self.dinos.append(Dino(self.displayGame))

    def update(self,dt,obz):
        num_alive = 0
        for d in self.dinos:
            if random.randint(0,3) == 1 :
                d.jump()
                
            d.update(dt,obz)
            if d.state == dino_alive:
                num_alive += 1

        return num_alive
