import pygame
import numpy
from defs import *
from obstacle import Obstacle
from dino import DinoGroup

def print_labels(dt,game_time,font,displayGame,num_iterations,num_alive):
    label = ['FPS: ','Game Time: ','Iterations: ','Alive: ']
    data = [round(1000/dt,1),round(game_time/1000,1),num_iterations,num_alive]
    coor = 10
    for x,y in zip(label,data):
        printData = font.render(x + str(y),1,font_color)
        displayGame.blit(printData,(10,coor+10))
        coor += 20

def startGame():
    pygame.init()
    displayGame = pygame.display.set_mode((width_game,height_game))
    pygame.display.set_caption('Test Dino')
    label_font = pygame.font.SysFont("monospace",font_size)

    bg = pygame.image.load(background_file)
    
    clock = pygame.time.Clock()

    obs = Obstacle(displayGame, width_game, 100, obstacle_speed)
    obs.initialize()
    dinos = DinoGroup(displayGame)

    gameRunning = 1
    dt = 0  
    game_time = 0
    num_iterations = 1
    num_alive = 0

    move_map = {pygame.K_SPACE: print('JUMP'),pygame.K_w: print('JUMP')}

    while gameRunning == 1:

        dt = clock.tick(fps)
        game_time += dt

        displayGame.blit(bg,(0,0))
        
        for event in pygame.event.get():
            pressed = pygame.key.get_pressed()
            if event.type == pygame.QUIT:
                gameRunning = 0
                sys.exit()
        
        obs.update(dt, game_time)
        num_alive = dinos.update(dt,obs.obz)

        if num_alive == 0:
            obs = Obstacle(displayGame, width_game, 100, obstacle_speed)
            obs.initialize()
            game_time = 0
            dinos.create_new_generation()
            num_iterations+=1
            #dino = Dino(displayGame)    
            

        print_labels(dt,game_time,label_font,displayGame,num_iterations,num_alive)
        pygame.display.update()

if __name__ == "__main__":
    startGame()