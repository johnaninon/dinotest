width_game = 960
height_game = 540
fps = 30

font_size = 18
font_color = (30,30,30)

background_file = 'BG2.png'
obstacle_file = 'Pipe2.png'
dino_file = 'Robin.png'

obstacle_speed = 300/1000
obstacle_speed_slow = 0
obstacle_speed_fast = 1
obstacle_done = 1
obstacle_moving = 0
obstacle_gap = 400
obstacle_first = width_game

dino_start_speed = -1.2
dino_start_X = 200
dino_start_Y = 428
dino_alive = 1
dino_dead = 0
gravity = 0.004

generation_size = 200
